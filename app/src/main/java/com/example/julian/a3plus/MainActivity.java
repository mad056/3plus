package com.example.julian.a3plus;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WebView webview = (WebView) findViewById(R.id.browser);
        webview.getSettings().setJavaScriptEnabled(true);

        webview.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");

        webview.setWebViewClient(new android.webkit.WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
               // webview.loadUrl("javascript:window.HtmlViewer.showHTML" +  "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                webview.loadUrl("javascript:window.HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });

        webview.loadUrl("http://www.student3plus.unsw.edu.au/search/course-list");
    }

    class MyJavaScriptInterface {

        private android.content.Context ctx;
        MyJavaScriptInterface(android.content.Context ctx) {
            this.ctx = ctx;
        }
        @JavascriptInterface
        public void showHTML(String html) throws JSONException {
            String stripped = Jsoup.parse(html).text();
            System.out.println(stripped);
            JSONArray jsonArray = new JSONArray(stripped.toString());
            for(int i=0; i<jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String jsonObjectAsString = jsonObject.getString("label");
                System.out.println(jsonObjectAsString);
            }

        }

    }
}

